using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Linq;
using System.Collections.Generic;

public class GamePole : MonoBehaviour  {

	public GameObject MainScript;

	public GameObject ePole, eNumber, eLetter, eComponentShip;

	public GameObject[,] Pole;

	private GameObject[] Numbers;

	private GameObject[] Letters;
	
	public GameObject MapDestination;

	public bool HideShip = false;

	public bool isManualShipAdd = false;

	int lengPole = 10;
	
	private int[] PoleShips;

	private int BiggestShip;

	private List<Ship> Ships = new List<Ship>();

	public int[] getItemShips() {
		return new int[] {0, 4, 3, 2, 1};
	}

	public void CopyPole() {
		if (MapDestination != null) {
			for (int Y = 0; Y < lengPole; Y++) {
				for (int X = 0; X < lengPole; X++) {
					MapDestination.GetComponent<GamePole>().Pole[X, Y].GetComponent<Chunks>().Index = Pole[X, Y].GetComponent<Chunks>().Index;

				}
			}
			MapDestination.GetComponent<GamePole>().Ships.Clear();
			MapDestination.GetComponent<GamePole>().Ships.AddRange(Ships);
		}
	}

	private bool isPoleFull() {
		int Amaunt = 0;

		foreach(int Ship in PoleShips) {
			Amaunt += Ship;
		}

		if (Amaunt != 0) {
			return true;
		}

		return false;
	}

	public void CleanPole() {

		PoleShips = getItemShips ();
		Ships.Clear ();
		for (int Y = 0; Y < lengPole; Y++) {
			for (int X = 0; X < lengPole; X++) {
				Pole[X, Y].GetComponent<Chunks>().Index = 0;
			}
		}
		
		foreach(GameObject componentShip in  GameObject.FindGameObjectsWithTag("Board")) {
			componentShip.GetComponent<ComponentShip>().ResertPostion();
		}
	}

	public void EnterRandomShips() {
		CleanPole ();
		int X, Y;
		int ship = BiggestShip;
		int Derection;

		while(isPoleFull()) {
			X = Random.RandomRange(0, 10);
			Y = Random.RandomRange(0, 10);
			Derection = Random.RandomRange(0, 1);

			if (isShipAdd(ship, Derection, X, Y)) {
				PoleShips[ship]--;
				if (PoleShips[ship] == 0) {
					ship--;
				}
			}
		}
	}

	void CreatePole() {
		Vector3 StartPole = transform.position;
		GeneratePoleLetter (StartPole);
		GeneratePoleNumbers (StartPole);
		GenerateMainPoleLetter (StartPole);
		GenerateComponentShips (StartPole);
	}
	
	private void GenerateComponentShips(Vector3 StartPole) {
		if (eComponentShip != null) {
		
			int[] ComponentPoleShips = getItemShips ();
			int ship = BiggestShip;

			float X = transform.position.x + 13;
			float Y = transform.position.y - 1;

			while(isPoleFull()) {

				eComponentShip.GetComponent<ComponentShip>().ItemPosition = new Vector3(X, Y, transform.position.z);
				if (gameObject != null) {
					eComponentShip.GetComponent<ComponentShip>().ParentPoles = gameObject;
				}
				eComponentShip.GetComponent<ComponentShip>().size = ship;
				GameObject CompomentShip = Instantiate(eComponentShip);
				X = X + ship + 1;
				PoleShips[ship]--;
				if (PoleShips[ship] == 0) {
					X = transform.position.x + 13;
					Y = Y - 2;
					ship--;
				}
			}
		}
	}

	private void GenerateMainPoleLetter(Vector3 StartPole) {
		float X = StartPole.x + 1;
		float Y = StartPole.y -1;

		Pole = new GameObject[lengPole, lengPole];
		
		for (int y = 0; y < lengPole; y++) {
			for (int x = 0; x < lengPole; x++) {
				Pole[x, y] = Instantiate(ePole);
				Pole[x, y].GetComponent<Chunks>().Index = 0;
				Pole[x, y].GetComponent<Chunks>().HideChank = HideShip;
				Pole[x, y].transform.position = new Vector3(X, Y, StartPole.z);
				
				if (HideShip) {
					Pole[x, y].GetComponent<PoleClick>().Parent = this.gameObject;
				}


				Pole[x, y].GetComponent<PoleClick>().CX = x;
				Pole[x, y].GetComponent<PoleClick>().CY = y;
				if (eComponentShip != null) {
					Pole[x, y].GetComponent<BoxCollider2D> ().size = new Vector2(0.1F, 0.1F);
				}
				X++;
			}
			X = StartPole.x + 1;
			Y--;
		}
	}

	private void GeneratePoleLetter(Vector3 StartPole) {
		float X = StartPole.x + 1;
		Letters = new GameObject[lengPole];
		for (int Letter = 0; Letter < lengPole; Letter++) {
			Letters[Letter] = Instantiate(eLetter);
			Letters[Letter].transform.position = new Vector3(X, StartPole.y, StartPole.z);
			Letters[Letter].GetComponent<Chunks>().Index = Letter;
			X++;
		}
	}

	private void GeneratePoleNumbers(Vector3 StartPole) {
		float Y = StartPole.y - 1;
		Numbers = new GameObject[lengPole];
		for (int Number = 0; Number < lengPole; Number++) {
			Numbers[Number] = Instantiate(eNumber);
			Numbers[Number].transform.position = new Vector3(StartPole.x, Y, StartPole.z);
			Numbers[Number].GetComponent<Chunks>().Index = Number;
			Y--;
		}
	}

	bool IsValidDeck(int X, int Y) {
		if ((X > -1) && (Y > -1) && (X < 10) && (Y < 10)) {
		
			int[] XX = new int[9],  YY = new int[9]; 

			XX[0] = X + 1; XX[1] = X;    	XX[2] = X - 1;
			YY[0] = Y + 1; YY[1] = Y + 1;	YY[2] = Y + 1;

			XX[3] = X + 1; XX[4] = X;		XX[5] = X - 1;
			YY[3] = Y; 	   YY[4] = Y;		YY[5] = Y;

			XX[6] = X + 1; XX[7] = X;		XX[8] = X - 1;
			YY[6] = Y - 1; YY[7] = Y - 1;	YY[8] = Y - 1;

			for (int I = 0; I < 9; I++) {
				if ((XX[I] > -1) && (YY[I] > -1) && (XX[I] < 10) && (YY[I] < 10)) {
					if (Pole[XX[I], YY[I]].GetComponent<Chunks>().Index != 0) {
						return false;
					}
				}
			}

			return true;
		}

		return false;
	}
	
	public Ship.Coord[] getShipCoords(int ShipType, int XX, int YY, int X, int Y) {
		Ship.Coord[] Result = new Ship.Coord[ShipType];
		for (int P = 0; P < ShipType; P ++) {
			if (IsValidDeck(X, Y)) {
				Result[P].X = X;
				Result[P].Y = Y;
			} else {
				return null;
			}

			X += XX;
			Y += YY;
		}

		return Result; 
	}

	Ship.Coord[] setShip(int ShipType, int Derection, int X, int Y) {
		Ship.Coord[] Result = new Ship.Coord[ShipType];

		if (IsValidDeck (X, Y)) {
			switch (Derection) {
				case 0:
					Result = getShipCoords(ShipType, 1, 0, X, Y);

					if (Result == null) {
						Result = getShipCoords(ShipType, -1, 0, X, Y);
					}
					break;
				case 1:
					Result = getShipCoords(ShipType, 0, 1, X, Y);
				
					if (Result == null) {
						Result = getShipCoords(ShipType, 0, -1, X, Y);
					}
					break;
			} 
			return Result;
		}
		return null;
	}

	public bool isShipAdd(int ShipType, int Derection, int X, int Y) {
		Ship.Coord[] ship = setShip (ShipType, Derection, X, Y);
		if (ship != null) {
			foreach (Ship.Coord T in ship) {
				Pole[T.X, T.Y].GetComponent<Chunks>().Index = 3;
				if (isManualShipAdd) {
					Pole[T.X, T.Y].AddComponent<ShipRotaion>();
					Pole[T.X, T.Y].GetComponent<ShipRotaion>().Parent = gameObject;
					Pole[T.X, T.Y].GetComponent<ShipRotaion>().CX = X;
					Pole[T.X, T.Y].GetComponent<ShipRotaion>().CY = Y;
					Pole[T.X, T.Y].GetComponent<BoxCollider2D> ().size = new Vector2(1F, 1F);
				}
			}


			Ship PoleShip = new Ship(ShipType, ship, Derection);
			Ships.Add(PoleShip);
			return true;
		}
		return false;
	}

	// Use this for initialization
	void Start () {
		PoleShips = getItemShips();
		BiggestShip = PoleShips.Max ();
		CreatePole ();
	}
	
	public void Click(int X, int Y) {
		if (MainScript != null) {
			MainScript.GetComponent<MainGame>().UserClick(X, Y);
		}
	}

	public bool Shoot(int X, int Y) {
		int PoleSelect = Pole [X, Y].GetComponent<Chunks> ().Index;
		bool Result = true;

		switch(PoleSelect) {
			case 0:
				Pole [X, Y].GetComponent<Chunks> ().Index = 2;
				Result = false;
				break;
			case 3:
				Pole [X, Y].GetComponent<Chunks> ().Index = 1;
				Result = true;
			break;
		}

		return Result;
	}

	bool CheckShoot(int X, int Y) {
		bool Result = false;
		foreach (Ship ship in Ships) {
			foreach (Ship.Coord coord in ship.ShipCoord) {
				if ((coord.X == X) && (coord.Y == Y)) {
					int CountKills = 0;
					foreach (Ship.Coord killcoord in ship.ShipCoord) {
						int TestBlock = Pole [X, Y].GetComponent<Chunks> ().Index;
						if (TestBlock == 1) {
							CountKills ++;
						}
					}

					if (CountKills == ship.ShipCoord.Length) {
						Result = true;
					} else {
						Result = false;
					}
					return Result;
				}		
			}
		}
		return Result;
	}

	public int LifeShip() {
		int countLife = 0;
		foreach (Ship ship in Ships) {
			foreach (Ship.Coord coord in ship.ShipCoord) {
				int TestBlock = Pole [coord.X, coord.Y].GetComponent<Chunks> ().Index;
				if (TestBlock == 3) {
					countLife ++;
				}
			}
		}
		return countLife;
	}

	public void Rotaion (int X, int Y) {
		List<Ship> treeList = new List<Ship>();
		List<Ship> ccc = new List<Ship>();

		foreach (Ship ship in Ships) {
			foreach (Ship.Coord coord in ship.ShipCoord) {
				if ((coord.X == X) && (coord.Y == Y)) {

					int FX = ship.ShipCoord [0].X;
					int FY = ship.ShipCoord [0].Y;

					int Derection = 1;
					if (ship.Derection == 1) {
						Derection = 0;
					}

					foreach (Ship.Coord killcoord in ship.ShipCoord) {
						Pole [killcoord.X, killcoord.Y].GetComponent<Chunks> ().Index = 0;
					}		

					if (setShip (ship.Type, Derection, FX, FY) == null) {
						foreach (Ship.Coord killcoord in ship.ShipCoord) {
							Pole [killcoord.X, killcoord.Y].GetComponent<Chunks> ().Index = 3;
						}
					} else {
						Ship.Coord[] ship2cc = setShip (ship.Type, Derection, FX, FY);

						foreach (Ship.Coord T in ship2cc) {
								Pole[T.X, T.Y].GetComponent<Chunks>().Index = 3;
								if (isManualShipAdd) {
									Pole[T.X, T.Y].AddComponent<ShipRotaion>();
									Pole[T.X, T.Y].GetComponent<ShipRotaion>().Parent = gameObject;
									Pole[T.X, T.Y].GetComponent<ShipRotaion>().CX = X;
									Pole[T.X, T.Y].GetComponent<ShipRotaion>().CY = Y;
									Pole[T.X, T.Y].GetComponent<BoxCollider2D> ().size = new Vector2(1F, 1F);
								}
							}

						Ship Deck = new Ship(ship.Type, ship2cc, Derection);
						treeList.Add(ship);
						ccc.Add(Deck);
					}
				}
			}
		}

		foreach (Ship ship in treeList) {
			Ships.Remove(ship);
		}

		
		foreach (Ship ship in ccc) {
			Ships.Add(ship);
		}
	}

}
