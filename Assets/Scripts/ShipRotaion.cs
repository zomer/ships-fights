﻿using UnityEngine;
using System.Collections;

public class ShipRotaion : MonoBehaviour {
	public GameObject Parent = null;
	public int CX, CY;
	
	void OnMouseDown() {
		if (Parent != null) {
			Parent.GetComponent<GamePole>().Rotaion(CX, CY);
		}
	}	
}
