﻿using UnityEngine;
using System.Collections;

public class PoleClick : MonoBehaviour {
	public GameObject Parent = null;
	public int CX, CY;

	void OnMouseDown() {
		if (Parent != null) {
			Parent.GetComponent<GamePole>().Click(CX, CY);
		}
	}	

}
