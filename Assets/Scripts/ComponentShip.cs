using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ComponentShip : MonoBehaviour {

	public int size;
	public GameObject pole;
	public Vector3 ItemPosition;
	public GameObject ParentPoles;
	private Vector3 startPosition;
	private GameObject[] ComponentItems;
	private Vector3 screenPoint;
	private Vector3 offset;

	bool moveStart = false;
	int X = -10, Y = -10;

	void Start () {
		float X = ItemPosition.x;
		ComponentItems = new GameObject[size]; 

		for (int Item = 0; Item < size; Item++) {
			ComponentItems [Item] = Instantiate (pole);
			ComponentItems [Item].transform.position = new Vector3 (X, ItemPosition.y, ItemPosition.z);
			ComponentItems [Item].GetComponent<SpriteRenderer> ().sortingOrder = 2;
			ComponentItems [Item].transform.parent = gameObject.transform;
			ComponentItems [Item].GetComponent<Chunks> ().Index = 3;
			ComponentItems [Item].GetComponent<BoxCollider2D> ().size = new Vector2(0.65F, 0.65F);
			X++;
		}

		startPosition = gameObject.transform.position;
	}

	void OnTriggerStay2D(Collider2D other) {
		if (other.gameObject.GetComponent<PoleClick> () != null) {
			moveStart = true;
			PoleClick pole = other.gameObject.GetComponent<PoleClick> ();

			X = pole.CX;
			Y = pole.CY;
		}
	}
	
	void OnMouseDown() {
		screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
		offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
	}

	void OnMouseUp() {
		if (ParentPoles.GetComponent<GamePole> ().isShipAdd (size, 0, X, Y) && moveStart) {
			foreach (GameObject obj in gameObject.GetComponent<ComponentShip> ().ComponentItems) {
				obj.GetComponent<Chunks> ().Index = 4;
				obj.GetComponent<SpriteRenderer> ().sortingOrder = 0;
			}
		}

		transform.position = startPosition;
	}
	
	void OnMouseDrag(){
		Vector3 cursorPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
		Vector3 cursorPosition = Camera.main.ScreenToWorldPoint(cursorPoint) + offset;
		transform.position = cursorPosition;
	}

	public void ResertPostion() {
		if (gameObject != null && gameObject.GetComponent<ComponentShip> () != null) {
			foreach(GameObject obj in gameObject.GetComponent<ComponentShip> ().ComponentItems) {
				obj.GetComponent<Chunks> ().Index = 3;
				obj.GetComponent<SpriteRenderer> ().sortingOrder = 2;
			}
			transform.position = startPosition;
		}
	}
}
