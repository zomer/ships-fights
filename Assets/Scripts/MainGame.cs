﻿	using UnityEngine;
using System.Collections;

public class MainGame : MonoBehaviour {

	public int GameMode = 0;
	public GameObject PlayerPole, ComputerPole, Player, ManualAddPole;

	Texture2D Btn;
	Texture2D BtnHover;
	Rect LocationButton;
	Camera cam;
	bool whoseMove = true;

	void OnGUI() {
		float CenterScreenX = Screen.width / 2;
		float CenterScreenY = Screen.height / 2;

		GamePole PlayerPoleControle = PlayerPole.GetComponent<GamePole> ();
		GamePole ManualAddPoleComponent = ManualAddPole.GetComponent<GamePole> ();

		switch (GameMode) {
		case 0:
			
			cam = GetComponent<Camera> ();
			cam.orthographicSize = 8;
			
			this.transform.position = new Vector3 (0, 0, -10);
			
			// Start game button
			LocationButton = new Rect (CenterScreenX - 280, CenterScreenY - 170, 500, 200);
			
			GUI.backgroundColor = Color.clear;
			
			Btn = (Texture2D)Resources.Load ("Images/Menu/StartGameBtn", typeof(Texture2D));
			if (GUI.Button (LocationButton, new GUIContent (Btn))) {
				PlayerPoleControle.CleanPole ();
				GameMode = 1;
			}
			
			// Quit game button
			LocationButton = new Rect (CenterScreenX - 280, CenterScreenY + 10, 500, 200);
			
			Btn = (Texture2D)Resources.Load ("Images/Menu/QuitGameBtn", typeof(Texture2D));
			if (GUI.Button (LocationButton, new GUIContent (Btn))) {
				Application.Quit ();
			}
			
		break;	
		case 1:
			this.transform.position = new Vector3 (46, 0, -10);
			cam = GetComponent<Camera> ();
			GUI.backgroundColor = Color.clear;

			// Main menu button
			LocationButton = new Rect (CenterScreenX - 400, -10, 110, 50);
			Btn = (Texture2D)Resources.Load ("Images/Menu/MenuBtn", typeof(Texture2D));
			if (GUI.Button (LocationButton, Btn)) {
				GameMode = 0;
			}

			// Generate random ships button
			LocationButton = new Rect (CenterScreenX - 400, 20, 250, 50);
			Btn = (Texture2D)Resources.Load ("Images/Menu/GenerateShipsBtn", typeof(Texture2D));
			if (GUI.Button (LocationButton, Btn)) {
				PlayerPoleControle.EnterRandomShips ();
			}
	
			// Add manually ships on pole button
			LocationButton = new Rect (CenterScreenX - 407, 50, 250, 50);
			Btn = (Texture2D)Resources.Load ("Images/Menu/HandAddShipsBtn", typeof(Texture2D));
			if (GUI.Button (LocationButton, Btn)) {
				ManualAddPoleComponent.CleanPole ();
				GameMode = 2;
			}
		
			// Start game button
			if (PlayerPoleControle.LifeShip () == 20) {
				LocationButton = new Rect (CenterScreenX - 80, -10, 230, 150);
				Btn = (Texture2D)Resources.Load ("Images/Menu/GameBtn", typeof(Texture2D));
				if (GUI.Button (LocationButton, Btn)) {
					PlayerPole.GetComponent<GamePole> ().CopyPole ();
					ComputerPole.GetComponent<GamePole> ().EnterRandomShips ();
					GameMode = 3;
				}
			}
			
		break;
		case 2:
			this.transform.position = new Vector3(32, -25, -10);
			GUI.backgroundColor = Color.clear;
	
			LocationButton = new Rect (CenterScreenX - 400, -10, 110, 50);
			Btn = (Texture2D)Resources.Load ("Images/Menu/MenuBtn", typeof(Texture2D));
			if (GUI.Button (LocationButton, Btn)) {
				ManualAddPoleComponent.CleanPole();
				GameMode = 0;
			}
			
			LocationButton = new Rect (CenterScreenX - 400, 20, 250, 50);
			Btn = (Texture2D)Resources.Load ("Images/Menu/GenerateShipsBtn", typeof(Texture2D));
			if (GUI.Button (LocationButton, Btn)) {
				PlayerPoleControle.CleanPole ();
				ManualAddPoleComponent.CleanPole();
				GameMode = 1;
			}

			// Clean ships on pole button
			LocationButton = new Rect (CenterScreenX - 410, 50, 250, 50);
			Btn = (Texture2D)Resources.Load ("Images/Menu/CleanShipsBtn", typeof(Texture2D));
			if (GUI.Button (LocationButton, Btn)) {
				if (ManualAddPoleComponent != null) {
					ManualAddPoleComponent.CleanPole();
				}
			}

			// Start game button
			if (ManualAddPoleComponent.LifeShip () == 20) {
				LocationButton = new Rect (CenterScreenX - 80, -10, 230, 150);
				Btn = (Texture2D)Resources.Load ("Images/Menu/GameBtn", typeof(Texture2D));
				if (GUI.Button (LocationButton, Btn)) {
					ManualAddPoleComponent.GetComponent<GamePole> ().CopyPole ();
					ComputerPole.GetComponent<GamePole> ().EnterRandomShips ();
					GameMode = 3;
				}
			}
			break;
		case 3:
			this.transform.position = new Vector3 (90, 0, -10);

			GUI.backgroundColor = Color.clear;
			
			LocationButton = new Rect (CenterScreenX - 400, -10, 110, 50);
			Btn = (Texture2D)Resources.Load ("Images/Menu/MenuBtn", typeof(Texture2D));
			if (GUI.Button (LocationButton, Btn)) {
				GameMode = 0;
			}
			
			LocationButton = new Rect (CenterScreenX - 400, 20, 250, 50);
			Btn = (Texture2D)Resources.Load ("Images/Menu/GenerateShipsBtn", typeof(Texture2D));
			if (GUI.Button (LocationButton, Btn)) {
				PlayerPoleControle.CleanPole ();
				GameMode = 1;
			}

			// Add manually ships on pole button
			LocationButton = new Rect (CenterScreenX - 407, 50, 250, 50);
			Btn = (Texture2D)Resources.Load ("Images/Menu/HandAddShipsBtn", typeof(Texture2D));
			if (GUI.Button (LocationButton, Btn)) {
				ManualAddPoleComponent.CleanPole ();
				GameMode = 2;
			}

			break;
			case 4:
			this.transform.position = new Vector3(91, -21, -10);
			GUI.backgroundColor = Color.clear;
			
			LocationButton = new Rect (CenterScreenX - 400, -5, 110, 70);
			Btn = (Texture2D)Resources.Load ("Images/Menu/MenuBtn", typeof(Texture2D));
			if (GUI.Button (LocationButton, Btn)) {
				GameMode = 0;
			}
			
			LocationButton = new Rect (CenterScreenX - 400, -10, 250, 150);
			Btn = (Texture2D)Resources.Load ("Images/Menu/GenerateShipsBtn", typeof(Texture2D));
			if (GUI.Button (LocationButton, Btn)) {
				PlayerPoleControle.CleanPole ();
				GameMode = 1;
			}
			break;
			case 5:
				this.transform.position = new Vector3(90, -42, -10);
				GUI.backgroundColor = Color.clear;
				
				LocationButton = new Rect (CenterScreenX - 400, -5, 110, 70);
				Btn = (Texture2D)Resources.Load ("Images/Menu/MenuBtn", typeof(Texture2D));
				if (GUI.Button (LocationButton, Btn)) {
					GameMode = 0;
				}
				
				LocationButton = new Rect (CenterScreenX - 400, -10, 250, 150);
				Btn = (Texture2D)Resources.Load ("Images/Menu/GenerateShipsBtn", typeof(Texture2D));
				if (GUI.Button (LocationButton, Btn)) {
					PlayerPoleControle.CleanPole ();
					GameMode = 1;
				}
			break;
		}
	}

	void ArtificialIntentLLigance() {
		if (!whoseMove) {
			int ShootX = Random.RandomRange(0, 10);
			int ShootY = Random.RandomRange(0, 10);
			whoseMove = !Player.GetComponent<GamePole>().Shoot(ShootX, ShootY);
		}
	}

	public void UserClick(int X, int Y) {
		if (whoseMove) {
			whoseMove = ComputerPole.GetComponent<GamePole>().Shoot(X, Y);
		}
	}

	void WhoWin() {
		int PC_Ship = ComputerPole.GetComponent<GamePole> ().LifeShip ();
		int User_Ship = Player.GetComponent<GamePole> ().LifeShip ();

		if (PC_Ship == 0) {
			GameMode = 4;
			this.transform.position = new Vector3(91, -21, -10);
		}

		if (User_Ship == 0) {
			GameMode = 5;
			this.transform.position = new Vector3(90, -42, -10);
		}
	}

	void Update () {
		if (GameMode == 3) {
			WhoWin();
			ArtificialIntentLLigance ();
		}

	}
}
