﻿using UnityEngine;
using System.Collections;

public class Ship : MonoBehaviour {

	private int type;

	private Coord[] shipCoord;

	private int derection;

	public struct Coord {
		public int X, Y;
	}

	public Ship (int type, Coord[] shipCoord, int derection)
	{
		this.type = type;
		this.shipCoord = shipCoord;
		this.derection = derection;
	}

	public int Derection {
		get {
			return this.derection;
		}
	}
	
	public Coord[] ShipCoord {
		get {
			return this.shipCoord;
		}
	}


	public int Type {
		get {
			return this.type;
		}
	}
}
