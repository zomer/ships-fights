﻿using UnityEngine;
using System.Collections;

public class Chunks : MonoBehaviour {

	public Sprite[] imgs;

	public int Index = 0;

	public bool HideChank = false;

	void ChangeImage() {
		if (imgs.Length > Index) {
			if (HideChank && Index == 3) {
				GetComponent<SpriteRenderer>().sprite = imgs[0];
			} else {
				GetComponent<SpriteRenderer>().sprite = imgs[Index];
			}
		}
	}
	
	void Start () {
		ChangeImage ();
	}
	
	void Update () {
		ChangeImage ();
	}

}
